//Módulos core de Node.js

//fs (Manejo de archivos)
//http (Crear un servidor o para poder hacer peticiones a otros servidores)
//path (Construcción de rutas hacía los archivos o directorios)
//events (Manejo de eventos)

//Importar un módulo en Node.js
//const modulo = require('modulo'); -> Node.js
//import modulo from modulo -> React

const fs = require('fs');

//Leer un archivo con fs (asincrono)
fs.readFile("saludo.txt", {encoding: "utf8"}, (err, data) => {
    if(!err){
        console.log(data);
    }else{
        console.log(err);
    }
});

const data = fs.readFileSync("saludo.txt", {encoding: "utf8"}); //Sincrono
console.log(data);

//Escribe en un archivo de forma asincrona y remplaza todo el contenido
fs.writeFile("saludo.txt", "Hello Omar", (err) => {
    if(err){
        console.log(err);
    }else{
        console.log("Se ha escrito en el archivo")
    }
});

//Agrega un nuevo contenido a partir del contenido original de forma asincrona
fs.appendFile("saludo.txt", " Agregamos palabras al final del archivo", (err) => {
    if(err){
        console.log(err);
    }else{
        console.log("Se ha escrito en el archivo")
    }
})

//Métodos Sincronos
// fs.readFileSync
// fs.writeFileSync
// fs.appendFileSync

//Métodos asincronos
// fs.readFile
// fs.writeFile
// fs.appendFile