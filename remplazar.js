const fs = require('fs');

//Leer un archivo con fs (asincrono)
fs.readFile("saludo.txt", {encoding: "utf8"}, (err, data) => {
    if(!err){
        let content = data;
        content = content.replace("Omar", "Armando");
        fs.writeFile("saludo.txt", content, (error) => {
            if(!error){
                console.log("Se ha escrito en el archivo");
            }
        });
    }else{
        console.log(err);
    }
});